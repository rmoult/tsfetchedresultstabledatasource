# TSFetchedResultsTableDataSource #


### Handling common functionality for UITableViewControllers that use NSFetchedResultsController ###

```
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.dataSource = [[TSFetchedResultsTableDataSource alloc] initWithTableView:self.tableView fetchedResultsController:[self fetchedResultsControllerForStatsInDateOrder]];
    
    self.dataSource.delegate = self;
    
    [self.dataSource reloadData];
}


- (NSFetchedResultsController *)fetchedResultsControllerForStatsInDateOrder {
    
    NSManagedObjectContext *context = xxx;
    
    NSFetchRequest *fetchRequest = xxx;
    
    return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
}


- (NSString *)cellReuseIdentifier {
    
    return @"StorybaordCellIdentifier";
}


- (void)configureCell:(UITableViewCell *)cell withObject:(NSManagedObject *)object {
    
    // configure cell
}
```


### How do I get set up? ###

pod 'TSFetchedResultsTableDataSource', :git => 'https://rmoult@bitbucket.org/rmoult/tsfetchedresultstabledatasource.git'

or download code and copy all c or h files into your project with a core data framework