//
//  TSFetchedResultsTableDataSource.h
//  MathSkills
//
//  Created by Richard Moult on 02/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


@protocol TSFetchedResultsTableDataSourceDelegate <NSObject>

- (NSString *)cellReuseIdentifierAtIndexPath:(NSIndexPath *)indexPath;

- (void)configureCell:(UITableViewCell *)cell withObject:(NSManagedObject *)object;

@optional

- (void)dataSourceDeleteObject:(NSManagedObject *)object;

@end




@interface TSFetchedResultsTableDataSource : NSObject

@property (nonatomic, weak) id<TSFetchedResultsTableDataSourceDelegate> delegate;
@property (nonatomic, strong, readonly) NSFetchedResultsController *fetchedResultsController;


- (id)initWithTableView:(UITableView *)tableView fetchedResultsController:(NSFetchedResultsController *)fetchedResultsController;

- (void)allowCellDeletionOnAllRows:(BOOL)allowDeletion;

- (void)reloadData;

- (NSManagedObject *)objectAtIndex:(NSIndexPath *)indexPath;

@end
