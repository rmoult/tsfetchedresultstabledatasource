//
//  TSFetchedResultsTableDataSource.m
//  MathSkills
//
//  Created by Richard Moult on 02/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import "TSFetchedResultsTableDataSource.h"


@interface TSFetchedResultsTableDataSource() <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, assign) BOOL canDeleteAllRows;
@end



@implementation TSFetchedResultsTableDataSource


- (id)init {

    return [self initWithTableView:nil fetchedResultsController:nil];
}


- (id)initWithTableView:(UITableView *)tableView fetchedResultsController:(NSFetchedResultsController *)fetchedResultsController {

    NSAssert(tableView, @"requires table view");
    NSAssert(fetchedResultsController, @"requires fetched results controller");

    self = [super init];

    if (self) {

        _tableView = tableView;
        _tableView.dataSource = self;
        _fetchedResultsController = fetchedResultsController;
        _fetchedResultsController.delegate = self;
    }

    return self;
}


- (NSManagedObject *)objectAtIndex:(NSIndexPath *)indexPath {

    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}


#pragma mark - table delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return [self.fetchedResultsController sections].count;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    id<NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];

    return [sectionInfo name];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    id  sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[self.delegate cellReuseIdentifierAtIndexPath:indexPath] forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self.delegate configureCell:cell withObject:object];
}


#pragma mark - fetched results controller

- (void)allowCellDeletionOnAllRows:(BOOL)allowDeletion {

    self.canDeleteAllRows = allowDeletion;
}

- (void)reloadData {

    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    [self.tableView reloadData];
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {

    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        default:
            NSLog(@"not yet handling these types of updates");
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {

    [self.tableView endUpdates];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    return self.canDeleteAllRows;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {

        if ([self.delegate respondsToSelector:@selector(dataSourceDeleteObject:)]) {

            NSManagedObject *object = [self objectAtIndex:indexPath];

            [self.delegate dataSourceDeleteObject:object];
        }
    }
}

@end
