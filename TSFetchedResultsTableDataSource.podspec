
Pod::Spec.new do |s|

s.name         = "TSFetchedResultsTableDataSource"
s.version      = "1.1.0"
s.summary      = "Handles common functionality for UITableViewController combined with fetch results controllers"
s.author       = "Richard Moult"
s.homepage     = "https://rmoult@bitbucket.org/rmoult/tsfetchedresultstabledatasource"
s.license      = { :type => 'The MIT License (MIT)', :text => <<-LICENSE
Copyright (c) 2015 Richard Moult.
LICENSE
}
s.source_files = '*.{h,m}'
s.requires_arc = true
s.platform     = :ios, '7.1'
s.source       = {:git => "https://rmoult@bitbucket.org/rmoult/tsfetchedresultstabledatasource", :tag=> '1.1.0'}
s.frameworks   = 'CoreData'

end
